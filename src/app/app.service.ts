import { Injectable } from '@angular/core';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
 
@Injectable()
export class MyService {
 
  
  
    constructor(private _scrollToService: ScrollToService) { }
 

  public triggerScrollTo() {
    
    const config: ScrollToConfigOptions = {
      target: 'how'
    };
 
    this._scrollToService.scrollTo(config);
  }

  public triggerScroll() {
    
    const config: ScrollToConfigOptions = {
      target: 'contact'
    };
 
    this._scrollToService.scrollTo(config);
  }






}